<?php 
namespace App\Models;
use CodeIgniter\Model;

class ProvaAlunoModel extends Model {

    protected $table = 'tbprovaaluno';
    protected $allowedFields = ['id_prova','id_aluno','realizada','nota'];

    public function listar($id = null){
        if($id === null){
            return $this->findAll();
        }
        return $this->asArray()->Where(['id_prova' => $id])->first(); 
    }

}