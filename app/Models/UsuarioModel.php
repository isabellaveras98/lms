<?php 
namespace App\Models;
use CodeIgniter\Model;

class UsuarioModel extends Model {

    protected $table = 'tbusuario';
    protected $primaryKey = 'id_usuario';
    protected $allowedFields = ['login_usuario','senha_professor', 'tipo'];

    public function listar($id = null){
        if($id === null){
            // tras todas ai info do banco quando nao tem id
            return $this->findAll();
        }
        // tras a info do banco om o id que foi informado
        return $this->asArray()->Where(['id_usuario' => $id])->first(); 
    }

    public function ListarPorTipo($tipo = null){
        if($tipo === null){
            // tras todas ai info do banco quando nao tem id
            return $this->findAll();
        }
        // tras a info do banco om o id que foi informado em forma de array
        return $this->asArray()->Where(['tipo' => $tipo])->first();    
    }

}