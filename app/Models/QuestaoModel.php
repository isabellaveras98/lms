<?php 
namespace App\Models;
use CodeIgniter\Model;

class QuestaoModel extends Model {

    protected $table = 'tbquestao';
    protected $primaryKey = 'id_questao';
    protected $allowedFields = ['tag','enunciado'];

    public function listar($id = null){
        if($id === null){
            return $this->findAll();
        }
        return $this->asArray()->Where(['id_questao' => $id])->first(); 
    }

    
}