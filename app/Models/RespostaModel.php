<?php 
namespace App\Models;
use CodeIgniter\Model;

class RespostaModel extends Model {

    protected $table = 'tbresposta';
    protected $primaryKey = 'id_resposta';
    protected $allowedFields = ['id_resposta','id_questao','certa', 'resposta'];

    public function listar($id = null){
        if($id === null){
            return $this->findAll();
        }
        return $this->asArray()->Where(['id_resposta' => $id])->first(); 
    }
}