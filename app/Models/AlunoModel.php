<?php 
namespace App\Models;
use CodeIgniter\Model;

class AlunoModel extends Model {

    public function materias(){

        $db = \Config\Database::connect();
        $builder = $db->table('tbconteudo');
        $builder->select('nome_professor, nome_disciplina, titulo');
        $builder->join('tbprofessor', 'tbprofessor.id_professor = tbconteudo.id_professor');
        $builder->join('tbdisciplina', 'tbdisciplina.id_disciplina = tbconteudo.id_disciplina');
        $query = $builder->get();
        
        return $query->getResultArray();
    }

    public function atividades(){

        $id = session()->get('id_aluno');

        $db = \Config\Database::connect();
        $builder = $db->table('tbatividadealuno');
        $builder->select('nome_professor, nome_disciplina, atividade')->where('id_aluno ='.$id.'');
        $builder->join('tbatividadeprof', 'tbatividadeprof.id_atividadeprof = tbatividadealuno.id_atividadeprof');
        $builder->join('tbdisciplina','tbdisciplina.id_disciplina = tbatividadeprof.id_disciplina');
        $builder->join('tbprofessor','tbprofessor.id_professor = tbatividadeprof.id_professor');
        $query = $builder->get();

        return $query->getResultArray();
    }

    /* public function provas(){

        $id = session()->get('id');

        $db = \Config\Database::connect();
        $builder = $db->table('tbprovaaluno');
        $builder->select('nome_professor, nome_disciplina, nota')->where('id_aluno ='.$id.'');
        $builder->join('tbprovaprof', 'tbprovaprof.id_provaprof = tbprovaaluno.id_provaprof');
        $builder->join('tbprofessor', 'tbprofessor.id_professor = tbprovaprof.id_professor');
        $builder->join('tbdisciplina', 'tbdisciplina.id_disciplina = tbprovaprof.id_disciplina');
        $query = $builder->get();
        $provas = $query->getResultArray();

        return $provas;
    } */
    
}