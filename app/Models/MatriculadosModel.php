<?php 
namespace App\Models;
use CodeIgniter\Model;

class MatriculadosModel extends Model {

    protected $table = 'matriculados';
    protected $primaryKey = 'id_matriculados';
    protected $allowedFields = ['id_aluno','id_disciplina'];

    public function listar($id = null){
        if($id === null){
            return $this->findAll();
        }
        return $this->asArray()->Where(['id_matriculados' => $id])->first(); 
    }
    
}