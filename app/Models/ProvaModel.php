<?php 
namespace App\Models;
use CodeIgniter\Model;

class ProvaModel extends Model {
    protected $table = 'tbprova';
    protected $primaryKey = 'id_prova';
    protected $allowedFields = ['id_prova','questao1','questao2', 'questao3','questao4','questao5', 'id_disciplina'];

    public function todas($id = null){
        if($id === null){
            return $this->findAll();
        }
        return $this->asArray()->Where(['id_prova' => $id])->first(); 
    }


    

}