<?php

namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model
{

    public function VerificaTipo($user)
    {

        if ($user[0]['tipo'] == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function ValidaLogin($user)
    {
        $db = \Config\Database::connect();

        $sql = "select * from db_lms.tbusuario where login_usuario = ?";
        $userBanco = $db->query($sql, $user['email'])->getResultArray();
        print_r($userBanco);

        if ($this->VerificaTipo($userBanco)) {
            $sql = "select * from db_lms.tbaluno as a left join db_lms.tbusuario as b on a.login_aluno = b.login_usuario where b.login_usuario = ?";
            $userBanco = $db->query($sql, $user['email'])->getResultArray();
            print_r($userBanco);
        } else {
            $sql = "select * from db_lms.tbprofessor as a left join db_lms.tbusuario as b on a.login_professor = b.login_usuario where b.login_usuario = ?";
            $userBanco = $db->query($sql, $user['email'])->getResultArray();
            #print_r($userBanco);
        }

        if ($user['password'] === $userBanco[0]['senha_usuario']) {
            return $userBanco;
        } else {
            return "Usuário inexistente";
        }
    }

    public function Registra($user)
    {

        if ($user['radio'] == "aluno") {
            $tipo = "1";
        } else {
            $tipo = "0";
        }

        $db = \Config\Database::connect();

        $sql = "INSERT INTO db_lms.tbusuario (login_usuario, senha_usuario, tipo) VALUES (:email:, :senha:, :tipo:)";

        $userBanco = $db->query($sql, [
            'email' => $user['email'],
            'senha' => $user['password'],
            'tipo' =>  $tipo
        ])->getResultArray();

        if ($tipo == 1) {

            $sql = "INSERT INTO db_lms.tbaluno (nome_aluno, login_aluno, senha) VALUES (:nome:, :email:, :senha:)";

            $userprofessor = $db->query($sql, [
                'nome' => $user['nome'],
                'email' => $user['email'],
                'senha' =>  $user['password']
            ])->getResultArray();
        } else {
            $sql = "INSERT INTO db_lms.tbprofessor (login_professor, nome_professor, senha_professor) VALUES (:email:, :nome:, :senha:)";

            $useraluno = $db->query($sql, [
                'email' => $user['email'],
                'nome' => $user['nome'],
                'senha' =>  $user['password']
            ])->getResultArray();
        }

        return $userBanco;
    }
}
