<?php 
namespace App\Models;
use CodeIgniter\Model;

class MateriaModel extends Model {

    protected $table = 'tbdisciplina';
    protected $primaryKey = 'id_disciplina';
    protected $allowedFields = ['id_disciplina','id_professor','nome_disciplina','resumo_disciplina'];

    public function listar($id = null){
        if($id === null){
            return $this->findAll();
        }
        return $this->asArray()->Where(['id_disciplina' => $id])->first(); 
    }
    
}