<?php 
namespace App\Models;
use CodeIgniter\Model;

class ProfessorModel extends Model {

    protected $table = 'tbprofessor';
    protected $primaryKey = 'id_professor';
    protected $allowedFields = ['id_professor','nome_professor','login_professor','senha_professor'];

    public function listar($id = null){
        if($id === null){
            return $this->findAll();
        }
        return $this->asArray()->Where(['id_professor' => $id])->first(); 
    }

}