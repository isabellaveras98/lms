<form class="text-center" style="
    margin-left: 35%;
    margin-top: 2%;
"   action="../Questao/salvarQuestao" method="post">

<!-- PRIMEIRO CRIAR AS QUESTOES DEPOIS AS RESPOSTAS
    quando setado certa gravar 'S' se nao setado grabar 'N'
-->

  <div class="form-group ">
    <h3>Criar Uma Nova Questão</h3> 
    <label for="enunciado">Enunciado da Questão</label>
    <textarea class="form-control"name="enunciado"  id="enunciado" rows="3"></textarea>
    <label class="form-check-label" for="tag">Tag/Conteúdo associado</label>
    <input type="text" name="tag" class="form-control" id="tag" >
  </div>

  <h3>Respostas</h3> 
  <div class="form-group">
    <label for="resposta1">Resposta 1</label>
    <input type="text" class="form-control" name="resposta1" >
    <div class="form-check">
        <input type="checkbox" class="form-check-input" name="certa1">
        <label class="form-check-label" for="exampleCheck1">Certa</label>
    </div>
  </div>
  <div class="form-group">
    <label for="resposta2">Resposta 2</label>
    <input type="text" class="form-control" name="resposta2" >
    <div class="form-check">
        <input type="checkbox" class="form-check-input" name="certa2">
        <label class="form-check-label" for="exampleCheck1">Certa</label>
  </div>
  </div>
  <div class="form-group">
    <label for="resposta3">Resposta 3</label>
    <input type="text" class="form-control" name="resposta3" >
    <div class="form-check">
        <input type="checkbox" class="form-check-input" name="certa3">
        <label class="form-check-label" for="exampleCheck1">Certa</label>
  </div>
  </div>
  <div class="form-group">
    <label for="resposta4">Resposta 4</label>
    <input type="text" class="form-control" name="resposta4" >
    <div class="form-check">
        <input type="checkbox" class="form-check-input" name="certa4">
        <label class="form-check-label" for="exampleCheck1">Certa</label>
  </div>
  </div>
  <div class="form-group ">
 
  <button type="submit" class="btn btn-primary">Criar</button>
</form>
</div>