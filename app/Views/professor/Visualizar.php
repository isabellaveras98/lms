<?php echo \Config\Services::validation()->listErrors(); ?>
<div id="content" class="p-4 p-md-6">
<h2 class="mb-4">Visualizar </h2>

  <div class="col-md-5" style="margin-left: 30%; ">
    <form action="/Professor/salvar" method="post">
      <div class="form-group">
        <label for="nome"><b>Nome</b></label>
        <input type="text" disabled class="form-control" name="nome" id="nome" value="<?php echo isset($nome) ? $nome : '' ?>" >
      </div>

      <div class="form-group">
        <label for="login"><b>login</b></label>
        <input type="text" disabled class="form-control" name="login" id="login" aria-describedby="emailHelp" value="<?php echo isset($login) ? $login : ''?>" >
      </div>
    </form>
  </div>
</div>