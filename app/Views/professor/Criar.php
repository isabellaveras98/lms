<div id="content" class="p-4 p-md-6">
  <h2 class="mb-4">Criar </h2>

  <div class="col-md-5" style="margin-left: 30%; ">
    <form action="../public/Professor/Salvar" method="post">
      <div class="form-group">
        <label for="nome"><b>Nome</b></label>
        <input type="text" class="form-control" name="nome" id="nome" value="<?= old('nome') ?>">
      </div>

      <div class="form-group">
        <label for="login"><b>login</b></label>
        <input type="text" class="form-control" name="login" id="login" aria-describedby="emailHelp" value="<?= old('login') ?>">
      </div>

      <div class="form-group">
        <label for="senha"><b>Senha</b></label>
        <input type="password" class="form-control" name="senha" id="senha">
      </div>

      <input type="hidden" name="id" value=""></input>

      <div class="form-group">
        <input type="submit" value="Salvar" class="btn btn-primary"></input>
      </div>

    </form>
  </div>
  <?php if (isset($erro)) : ?>
    <p><?= $erro->listErrors(); ?> </p>
  <?php endif; ?>
</div>