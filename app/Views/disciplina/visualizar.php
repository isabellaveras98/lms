<?php echo \Config\Services::validation()->listErrors(); ?>
<div id="content" class="p-4 p-md-6">
<h2 class="col-md-5" style="margin-left: 30%; ">Detalhes da Disciplina </h2>

  <div class="col-md-5" style="margin-left: 30%; ">
    <form action="/Materia/salvar" method="post">
      <div class="form-group">
        <label for="nome"><b>Disciplina</b></label>
        <input type="text" disabled class="form-control" name="nome" id="nome" value="<?php echo isset($nome) ? $nome : '' ?>" >
      </div>

      <div class="form-group">
        <label for="aluno"><b>ID Aluno</b></label>
        <input type="text" disabled class="form-control" name="aluno" id="aluno" value="<?php echo isset($aluno) ? $aluno : ''?>" >
      </div>

      <div class="form-group d-grid gap-2 d-md-block">
        <a class="btn text-white" style="background-color: #669999" href="/lms/public/materia/index" role="button">Voltar</a>
      </div>
    </form>
  </div>
</div>