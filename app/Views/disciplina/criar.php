<div id="content" class="p-4 p-md-6">
  <h2 class="col-md-5" style="margin-left: 30%; ">Cadastrar Disciplina </h2>

  <div class="col-md-5" style="margin-left: 30%; ">
    <form action="/lms/public/Disciplina/Salvar" method="post">
      <div class="form-group">
        <label for="nome"><b>ID Disciplina</b></label>
        <input type="text" class="form-control" name="nome" id="nome" value="<?= old('id_disciplina') ?>">
      </div>

      <div class="form-group">
        <label for="aluno"><b>ID Aluno</b></label>
        <input type="text" class="form-control" name="aluno" id="aluno"  value="<?= old('id_aluno') ?>">
      </div>

      <div class="form-group d-grid gap-2 d-md-block">
        <a class="btn text-white" style="background-color: #669999" href="../professor" role="button">Voltar</a>
        <input type="submit" value="Salvar" class="btn text-white" style="background-color: #669999" role="button"></input>
      </div>

    </form>
  </div>
  <?php if (isset($erro)) : ?>
    <p><?= $erro->listErrors(); ?> </p>
  <?php endif; ?>
</div>