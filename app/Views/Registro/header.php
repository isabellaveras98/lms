<!doctype html>
<html lang="en">

<head>
  <title>E-Study</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
  <link rel="icon" type="image/x-icon" href="<?php echo base_url('assets/img/logo2.jpeg') ?>" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo 'http://localhost/lms/public/assets/css/style.css' ?>">
  <link rel="stylesheet" href="<?php echo 'http://localhost/lms/public/assets/css/aluno.css' ?>">
  <link rel="stylesheet" href="https://code.jquery.com/jquery-3.5.1.js">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js">
</head>

<body>

  <div class="col-md-12" id="topo">
    <div class="row">
      <img src="assets/img/logo1.jpeg" style="width: 200px" id="imagemLogo" >
    </div>
  </div>

  <div class="wrapper d-flex align-items-stretch">

    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 mt-5 pt-3 pb-3 bg-white from-wrapper">
          <div class="container">
            <h3>Cadastro</h3>
            <hr>
            <form class="" action="Registro/register" method="post">
              <div class="row">
                <div class="col-12">
                  <div class="col-12 col-sm-12">
                    <label for="firstname">Nome</label>
                    <input type="text" class="form-control" name="nome" id="nome" value="<?= set_value('Nome') ?>">
                  </div>
                </div>
                <div class="col-12">
                  <div class="col-12 col-sm-12">
                    <label for="email">E-mail</label>
                    <input type="text" class="form-control" name="email" id="email" value="<?= set_value('email') ?>">
                  </div>
                </div>
                </br></br></br></br>
                <div class="col-12">
                  <div class="form-group">
                    <input type="radio" id="professor" name="radio" value="professor">
                    <label for="professor">Eu sou Professor</label>
                    <input type="radio" id="aluno" name="radio" value="aluno">
                    <label for="aluno">Eu sou Aluno</label><br>
                  </div>
                </div>
                <div class="col-12 col-sm-6">
                  <div class="col-12 col-sm-12">
                    <label for="password">Senha</label>
                    <input type="password" class="form-control" name="password" id="password" value="">
                  </div>
                </div>
                <div class="col-12 col-sm-6">
                  <div class="col-12 col-sm-12">
                    <label for="password_confirm">Confirmar Senha</label>
                    <input type="password" class="form-control" name="password_confirm" id="password_confirm" value="">
                  </div>
                </div>
                </br></br></br></br>
                <div class="row">
                  <div class="col-12 col-sm-12">
                    <button type="submit" class="btn btn-primary" style="margin-left:220px;">Cadastrar</button>
                    <a href="http://localhost/lms/public/Login" style="margin-left:80px">Já tem cadastro?</a>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>


</body>

</html>