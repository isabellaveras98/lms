        <!-- Page Content  -->
        <div id="content" class="p-4 p-md-6">
            <h2 class="mb-4">Área do Professor</h2>

            <div class="container">
                <div class="row">
                    <!-- Cadastrar disciplina-->
                    <div class="card" style="width: 20rem;">
                        <div class="card-body">
                            <h5 class="card-title">Cadastrar Disciplina</h5>
                            <p class="card-text">Aqui você pode esta cadastrando a sua disciplina.</p>
                            <button class="btn" style="background-color: #669999;">Entrar</button>
                        </div>
                    </div>

                    <!-- Cadastrar materia-->
                    <div class="card" style="width: 20rem;">
                        <div class="card-body">
                            <h5 class="card-title">Cadastrar Matéria</h5>
                            <p class="card-text">Aqui você pode esta cadastrando uma matéria.</p>
                            <button class="btn" style="background-color: #669999;">Entrar</button>
                        </div>
                    </div>

                    <!-- Cadastrar aluno-->
                    <div class="card" style="width: 20rem;">
                        <div class="card-body">
                            <h5 class="card-title">Cadastrar Aluno</h5>
                            <p class="card-text">Aqui você pode esta cadastrando um aluno na sua disciplina.</p>
                            <button class="btn" style="background-color: #669999;">Entrar</button>
                        </div>
                    </div>

                    <!-- Cadastrar prova-->
                    <div class="card" style="width: 20rem; margin-left: 20%;">
                        <div class="card-body">
                            <h5 class="card-title">Cadastrar Prova</h5>
                            <p class="card-text">Aqui você pode esta cadastrando uma prova para a sua disciplina.</p>
                            <button class="btn" style="background-color: #669999;">Entrar</button>
                        </div>
                    </div>

                    <!-- Cadastrar atividade-->
                    <div class="card" style="width: 20rem;">
                        <div class="card-body">
                            <h5 class="card-title">Cadastrar Atividade</h5>
                            <p class="card-text">Aqui você pode esta cadastrando as atividades referente a sua disciplina.</p>
                            <button class="btn" style="background-color: #669999;">Entrar</button>
                        </div>
                    </div>

                </div>
            </div>
            <!--fim-->
        </div>