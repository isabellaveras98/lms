<!doctype html>
<html lang="en">

<head>
  <title>E-Study</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo 'http://localhost/lms/public/assets/css/style.css' ?>">
  <link rel="stylesheet" href="<?php echo 'http://localhost/lms/public/assets/css/Professor.css' ?>">
  <link rel="stylesheet" href="https://code.jquery.com/jquery-3.5.1.js">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js">

</head>

<body>

  <div class="col-md-12" id="topo">
    <div class="row">
      <img src="assets/img/logo1.jpeg" style="width: 200px" id="imagemLogo">

      <div class="dropdown dropdown-inline" data-toggle="tooltip" title="Memebr Profile" data-placement="top" id="imagemUser">
        <a href="http://localhost/lms/public/Professor" class="d-flex align-items-center" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <div class="d-flex flex-column text-right">
            <span class="text-muted font-weight-bold">
              <!--Logica para o nome-->Professor ADS</span>
          </div>
          <div class="symbol symbol-circle symbol-50 mr-3">
            <img alt="Pic" src="<?php echo 'http://localhost/lms/public/assets/img/avatar-default.png'?>" style="width: 80px" />
            <i class="symbol-badge bg-success"></i>
          </div>
        </a>
        <div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">
          ...
        </div>
      </div>
    </div>
  </div>



  <div class="wrapper d-flex align-items-stretch">
    <nav id="sidebar" class="active" style="height:100%">

      <button type="button" id="sidebarCollapse" class="btn" style="background-color: #669999">
        <i class="fa fa-bars"></i>

      </button>
      <!--Falta encontrar icones referente a cada menu-->
      <ul class="list-unstyled components mb-5">
        <li class="active">
          <a href="#"><span class="fa fa-home"></span> Atividades</a>
        </li>
        <li>
          <a href="#"><span class="fa fa-user"></span> Alunos</a>
        </li>
        <li>
          <a href="http://localhost/lms/public/Prova"><span class="fa fa-sticky-note"></span> Provas</a>
        </li>
        <li>
          <a href="http://localhost/lms/public/Materia"><span class="fa fa-cogs"></span> Materias</a>
        </li>
        <li>
          <a href="Login/logout" style="color: black;"><i class="fa fa-sign-out"></i> Sair</a>
        </li>
      </ul>

      <div class="footer">

      </div>
    </nav>