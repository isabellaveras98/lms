
<div id="content" class="p-4 p-md-6">
<h2 class="col-md-5 text-center" style="margin-left: 25%; ">Provas </h2>

    <!-- <a href="/lms/public/materia/criar" class="btn text-white" style="background-color: #669999; margin-left: 61.5%;" title="Cadastrar nova disciplina">+ Nova Disciplina</a><br/> -->

    <table id="example" class="table table-striped table-bordered nowrap" style="width:50%; margin-left: 25%; margin-top: 2%;">
        <tr>
            <th class="text-white text-center" style="background-color: #669999;">ID</th>
            <th class="text-white text-center" style="background-color: #669999;">Enunciado</th>
            <th class="text-white text-center" style="background-color: #669999;">Tag</th>
        </tr>
        <?php if (!empty($data) && is_array($data)) : ?>
            <?php foreach ($data as $prova) : ?>
                <tr>
                    <td class="text-center"><span> <?php echo $prova['id_questao'] ?> </span></td>
                    <td class="text-center"><span> <?php echo $prova['enunciado'] ?> </span></td>
                    <td class="text-center"><span> <?php echo $prova['tag'] ?> </span></td>

                </tr>
            <?php endforeach; ?>
        <?php else : ?>
            <tr>
                <td>  <h3 class="mb-4">Nenhum registro encontrado.</h2></td>
            <tr>
            <?php endif; ?>
    </table>
</div>