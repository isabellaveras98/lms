<div id="content" class="p-4 p-md-6">
  <h2 class="col-md-5" style="margin-left: 30%; ">Cadastrar Disciplina </h2>

  <div class="col-md-5" style="margin-left: 30%; ">
    <form action="/lms/public/Materia/Salvar" method="post">
      <div class="form-group">
        <label for="nome"><b>Disciplina</b></label>
        <input type="text" class="form-control" name="nome" id="nome" value="<?= old('nome') ?>">
      </div>

      <div class="form-group">
        <label for="resumo"><b>Resumo da Disciplina</b></label>
        <input type="text" class="form-control" name="resumo" id="resumo"  value="<?= old('resumo') ?>">
      </div>

      <!--<div class="form-group">
        <label for="senha"><b>Senha</b></label>
        <input type="password" class="form-control" name="senha" id="senha">
      </div>-->

      <input type="hidden" name="id" value=""></input>

      <div class="form-group">
      <label for="id_professor"><b>ID do professor</b></label>
        <input type="number" class="form-control" name="id_professor" value="<?= old('id_professor') ?>"></input>
      </div> 
      <!-- Por enquanto eu deixei assim com o ID do professor pra que não dê erro nas chaves do banco na hora do cadastro, mas podemos melhorar
      isso depois. -Flávia. -->

      <div class="form-group d-grid gap-2 d-md-block">
        <a class="btn text-white" style="background-color: #669999" href="../professor" role="button">Voltar</a>
        <input type="submit" value="Salvar" class="btn text-white" style="background-color: #669999" role="button"></input>
      </div>

    </form>
  </div>
  <?php if (isset($erro)) : ?>
    <p><?= $erro->listErrors(); ?> </p>
  <?php endif; ?>
</div>