
<div id="content" class="p-4 p-md-6">
<h2 class="col-md-5" style="margin-left: 30%; ">Editar Disciplina</h2>

  <div class="col-md-5" style="margin-left: 30%; ">
    <form action="/Materia/Salvar" method="post">
      <div class="form-group">
        <label for="nome"><b>Disciplina</b></label>
        <input type="text" class="form-control" name="nome" id="nome" value="<?php echo isset($nome)? $nome : set_value('nome')?>">
      </div>

      <div class="form-group">
        <label for="resumo"><b>Resumo</b></label>
        <input type="text" class="form-control" name="resumo" id="resumo" value="<?php echo isset($resumo) ? $resumo : set_value('resumo') ?>">
      </div>

      <!-- <div class="form-group">
        <label for="senha"><b>Senha</b></label>
        <input type="password" class="form-control" name="senha" id="senha" >
      </div> -->

      <div class="form-group">
        <label for="id_professor"><b>ID do professor</b></label>
        <input type="number" class="form-control" name="id_professor" value="<?php echo isset($id_professor) ? $id_professor : set_value('id_professor') ?>"></input>
      </div>

      <input type="hidden" name="id" value="<?php echo isset($id)? $id : '' ?>"></input>

      <div class="form-group d-grid gap-2 d-md-block">
        <a class="btn text-white" style="background-color: #669999" href="/lms/public/materia/index" role="button">Voltar</a>
        <input type="submit" value="Salvar" class="btn text-white" style="background-color: #669999" role="button"></input>
      </div>

    </form>
  </div>
  <?php if(isset($erro)): ?>
  <p><?= $erro->listErrors(); ?> </p>
<?php endif;?>
</div>

