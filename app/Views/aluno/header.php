<!doctype html>
<html lang="en">

<head>
  <title>E-Study</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
  <link rel="icon" type="image/x-icon" href="<?php echo base_url('assets/img/logo2.jpeg')?>"/>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo 'http://localhost/lms/public/assets/css/style.css' ?>">
  <link rel="stylesheet" href="<?php echo 'http://localhost/lms/public/assets/css/aluno.css' ?>">
  <link rel="stylesheet" href="https://code.jquery.com/jquery-3.5.1.js">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js">
</head>

<body>

  <div class="col-md-12" id="topo">
    <div class="row">
      <img src="assets/img/logo1.jpeg" style="width: 200px" id="imagemLogo">

      <div class="dropdown dropdown-inline" data-toggle="tooltip" title="Memebr Profile" data-placement="top" id="imagemUser">
        <a href="#" class="d-flex align-items-center" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          
          <div class="symbol symbol-circle symbol-50 mr-3">
            <img alt="Pic" src="assets/img/avatar-default.png" style="width: 80px" />
            <i class="symbol-badge bg-success"></i>
          </div>
        </a>
      </div>
    </div>
  </div>

  <div class="wrapper d-flex align-items-stretch">