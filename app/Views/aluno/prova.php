<form class="text-center" style="
    margin-left: 35%;
    margin-top: 2%;
" action="../Prova/salvar" method="post">
<div class="form-group">
    <label for="exampleFormControlSelect1">Questão 1</label>
    <select class="form-control" id="questao1" name="questao1">
      <option>Selecione...</option>
      <?php foreach ($data as $prova) : ?>
        <option value="<?php echo $prova['id_questao'] ?>"><?php echo $prova['enunciado'] ?></option>
      <?php endforeach; ?>
    </select>
  </div>

  <div class="form-group">
    <label for="exampleFormControlSelect1">Questão 2</label>
    <select class="form-control" id="questao2" name="questao2">
      <option>Selecione...</option>
      <?php foreach ($data as $prova) : ?>
        <option value="<?php echo $prova['id_questao'] ?>"><?php echo $prova['enunciado'] ?></option>
      <?php endforeach; ?>
    </select>
  </div>

<div class="form-group">
    <label for="exampleFormControlSelect1">Questão 3</label>
    <select class="form-control" id="questao3" name="questao3">
      <option>Selecione...</option>
      <?php foreach ($data as $prova) : ?>
        <option value="<?php echo $prova['id_questao'] ?>"><?php echo $prova['enunciado'] ?></option>
      <?php endforeach; ?>
    </select>
  </div>

  <div class="form-group">
    <label for="exampleFormControlSelect1">Questão 4</label>
    <select class="form-control" id="questao4" name="questao4">
      <option>Selecione...</option>
      <?php foreach ($data as $prova) : ?>
        <option value="<?php echo $prova['id_questao'] ?>"><?php echo $prova['enunciado'] ?></option>
      <?php endforeach; ?>
    </select>
  </div>

  <div class="form-group">
    <label for="exampleFormControlSelect1">Questão 5</label>
    <select class="form-control" id="questao5" name="questao5">
      <option>Selecione...</option>
      <?php foreach ($data as $prova) : ?>
        <option value="<?php echo $prova['id_questao'] ?>"><?php echo $prova['enunciado'] ?></option>
      <?php endforeach; ?>
    </select>
  </div>

  <div class="form-group">
    <label for="exampleFormControlSelect1">Disciplina</label>
    <select class="form-control" id="id_disciplina" name="id_disciplina">
      <option>Selecione...</option>
      <?php foreach ($disciplina as $prova) : ?>
        <option value="<?php echo $prova['id_disciplina'] ?>"><?php echo $prova['nome_disciplina'] ?></option>
      <?php endforeach; ?>
    </select>
  </div>

  <button type="submit" class="btn btn-primary">Criar Prova</button>
</form>
</div>