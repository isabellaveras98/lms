<div id="content" class="p-4 p-md-6">
            <h2 class="mb-4">Atividades para entregar</h2>
            <hr>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="inam" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <?php if (!empty($tbatividadealuno) && is_array($tbatividadealuno)) : ?>
                                <?php $n = 0;  for($i = 0; $i <= sizeof($tbatividadealuno); $i += $n){ if(!isset($tbatividadealuno[$n])){ break; }?>
                                <div class="carousel-item <?php if($i == 0){ echo "active"; } ?>">
                                    <div class="container">
                                        <div class="row">
                                            <?php for($j = 0; $j < sizeof($tbatividadealuno); $j++){?>
                                            <div class="col-sm-12 col-lg-4">
                                                <div class="card" style="width: 300px;margin: auto;">
                                                    <div class="card-body">
                                                        <h4 class="card-title"><?= $tbatividadealuno[$n]['nome_disciplina']?></h4>
                                                        <p class="card-text">Data de entrega: <?= date('d/m/Y', strtotime($tbatividadealuno[$n]['data_atividade']))?></p>    
                                                        <p class="card-text">Assunto: <?= $tbatividadealuno[$n]['atividade']?></p>    
                                                        <p class="card-text">Professor: <?= $tbatividadealuno[$n]['nome_professor']?> </p>    
                                                    </div>
                                                </div>   
                                            </div>
                                            <?php $n++; if(!isset($tbatividadealuno[$n])){ break; }}?>   
                                        </div> 
                                    </div>
                                </div><?php } ?>
                                
                            </div>
                            <a href="#inam" class="carousel-control-prev" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a href="#inam" class="carousel-control-next" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>

                            <?php else : ?>
                                <tr>
                                    <td>  <h3 class="mb-4" style=" margin: 0 auto;">Não possui nenhuma atividade para entregar</h2></td>
                                <tr>
                            <?php endif; ?>
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>
            <hr>

            <h2 class="mb-4">Provas a serem realizadas</h2>
            <hr>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="inam2" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <?php if (!empty($tbprovaaluno) && is_array($tbprovaaluno)) : ?>
                                <?php $n = 0;  for($i = 0; $i <= sizeof($tbprovaaluno); $i += $n){ if(!isset($tbprovaaluno[$n])){ break; }?>
                                <div class="carousel-item <?php if($i == 0){ echo "active"; } ?>">
                                    <div class="container">
                                        <div class="row">
                                            <?php for($j = 0; $j < 3; $j++){?>
                                            <div class="col-sm-12 col-lg-4">
                                                <div class="card" style="width: 300px;margin: auto;">
                                                    <div class="card-body">
                                                        <h4 class="card-title"><?= $tbprovaaluno[$n]['nome_disciplina']?></h4>
                                                        <p class="card-text">Data de realização: <?= date('d/m/Y', strtotime($tbprovaaluno[$n]['data_provaluno']))?></p>    
                                                        <p class="card-text">Nota: <?= $tbprovaaluno[$n]['nota'] ?></p>    
                                                        <p class="card-text">Professor: <?= $tbprovaaluno[$n]['nome_professor']?> </p>    
                                                    </div>
                                                </div>   
                                            </div>
                                            <?php $n++; if(!isset($tbprovaaluno[$n])){ break; } }?>   
                                        </div> 
                                    </div>
                                </div> <?php } ?>
                                
                            </div>
                            <a href="#inam2" class="carousel-control-prev" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a href="#inam2" class="carousel-control-next" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>

                            <?php else : ?>
                                <tr>
                                    <td>  <h3 class="mb-4" style=" margin: 0 auto;">Não possui nenhuma prova para ser feita</h2></td>
                                <tr>
                            <?php endif; ?>
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>
            <hr>

            <h2 class="mb-4">Últimas matérias adicionadas</h2>
            <hr>
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="inam3" class="carousel slide" data-ride="carousel"> 
                            <div class="carousel-inner">
                                <?php if (!empty($tbconteudo) && is_array($tbconteudo)) : ?>
                                <?php $n = 0;  for($i = 0; $i <= sizeof($tbconteudo); $i += $n){ if(!isset($tbconteudo[$n])){ break; }?>
                                <div class="carousel-item <?php if($i == 0){ echo "active"; } ?>">
                                    <div class="container">
                                        <div class="row">
                                            <?php for($j = 0; $j < 3; $j++){?>
                                            <div class="col-sm-12 col-lg-4">
                                                <div class="card" style="width: 300px;height:200px;margin: auto;">
                                                    <div class="card-body">
                                                        <h4 class="card-title"><?= $tbconteudo[$n]['nome_disciplina']?></h4>   
                                                        <p class="card-text">Assunto: <?= $tbconteudo[$n]['titulo']?></p>    
                                                        <p class="card-text">Professor: <?= $tbconteudo[$n]['nome_professor']?></p>    
                                                    </div>
                                                </div>   
                                            </div>
                                            <?php $n++; if(!isset($tbconteudo[$n])){ break;}}?> 
                                        </div>
                                    </div> 
                                </div><?php }?>
                                
                            </div>
                            <a href="#inam3" class="carousel-control-prev" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a href="#inam3" class="carousel-control-next" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>

                            <?php else : ?>
                                <tr>
                                    <td>  <h3 class="mb-4" style=" margin: 0 auto;">Nenhuma matéria adicionada</h2></td>
                                <tr>
                            <?php endif; ?>
                        </div>      
                    </div>              
                </div>     
            </div>
            <hr>
        </div>
    </div>