<?php

namespace App\Controllers;

use App\Models\{QuestaoModel, RespostaModel};
use CodeIgniter\Model;

class Questao extends BaseController
{




	public function criar()
	{

		echo view('commom/header');
		echo view('questao/criar');
		echo view('commom/footer');
	}

	public function salvarQuestao()
	{

		$modelQuestao = new QuestaoModel();		
		$enunciado = $this->request->getPost('enunciado');
		$tag = $this->request->getPost('tag');

		$dataQuestao = [
			'enunciado'  => $enunciado,
			'tag' =>  $tag,

		];


		$modelQuestao->save($dataQuestao);

		$this->salvarResposta();

		return redirect()->to('http://localhost/lms/public/Prova');
	}

	public function salvarResposta(){

		$db = \Config\Database::connect();

		$sql = "SELECT max(id_questao) FROM db_lms.tbquestao";
        $userBanco = $db->query($sql)->getResultArray();

		$modelResposta = new RespostaModel();
		$resposta = $this->request->getPost('resposta1');
		$certa = $this->request->getPost('certa1');

		if ($certa === "on") {
			$certa = 'S';
		} else {
			$certa = 'N';
		}

		$dataModel = [
			'id_questao' => $userBanco[0]['max(id_questao)'],
			'resposta'  => $resposta,
			'certa' =>  $certa,

		];


		$modelResposta->save($dataModel);

		$resposta = $this->request->getPost('resposta2');
		$certa = $this->request->getPost('certa2');

		if ($certa === "on") {
			$certa = 'S';
		} else {
			$certa = 'N';
		}



		$dataModel2 = [
			'id_questao' => $userBanco[0]['max(id_questao)'],
			'resposta'  => $resposta,
			'certa' =>  $certa,

		];


		$modelResposta->save($dataModel2);
		$resposta = $this->request->getPost('resposta3');
		$certa = $this->request->getPost('certa3');

		if ($certa === "on") {
			$certa = 'S';
		} else {
			$certa = 'N';
		}



		$dataModel3 = [
			'id_questao' => $userBanco[0]['max(id_questao)'],
			'resposta'  => $resposta,
			'certa' =>  $certa,

		];


		$modelResposta->save($dataModel3);

		$resposta = $this->request->getPost('resposta4');
		$certa = $this->request->getPost('certa4');

		if ($certa === "on") {
			$certa = 'S';
		} else {
			$certa = 'N';
		}

		$dataModel4 = [
			'id_questao' => $userBanco[0]['max(id_questao)'],
			'resposta'  => $resposta,
			'certa' =>  $certa,

		];


		$modelResposta->save($dataModel4);
	}

	public function listar($id = null)
	{

		$model =  new QuestaoModel();
		$data['id_questao'] = $model->listar($id);

		$model->select("id_questao, tag, enunciado");
		$materias = $model->listar();

		foreach ($materias as $materia) {
			$result[] = [
				$materia['enunciado'],
				$materia['id_questao'],
				$materia['tag']
			];
		}

		$data  =  [
			'data' => $materias
		];

		echo view('commom/header');
		echo view('questao/listar', $data);
		echo view('commom/footer');
	}
}
