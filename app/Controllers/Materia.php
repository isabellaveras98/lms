<?php

namespace App\Controllers;

use App\Models\MateriaModel;


class Materia extends BaseController
{
	public function index()
	{
	
		$model =  new MateriaModel();
		$model->select("id_disciplina, id_professor, nome_disciplina");
		$materias = $model->listar();

		foreach ($materias as $materia) {
			$result[] = [
				$materia['nome_disciplina'],
				$materia['id_disciplina'],
				$materia['id_professor']
			];
		}

		$data  =  [
			'data' => $materias
		];

		echo view('commom/header');
		echo view('materia/index', $data);
		echo view('commom/footer');

    }
    
    public function visualizar($id = null)
	{
		
		$model =  new MateriaModel();
		$data['id_disciplina'] = $model->listar($id);

		if (empty($data['id_disciplina'])) {
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Matéria não encontrada.');
		}
		// array com os dados do banco para a view
		$data = [
			'nome' => $data['id_disciplina']['nome_disciplina'],
			'resumo' => $data['id_disciplina']['resumo_disciplina'],
		];

		echo view('commom/header');
		echo view('materia/visualizar', $data);
		echo view('commom/footer');

	}

    public function criar()
	{
		$data = [];
		if (session()->has('erro')) {
			$data['erro'] = session('erro');
		}
		echo view('commom/header');
		echo view('materia/criar', $data);
		echo view('commom/footer');
	}

    public function Salvar($id = null)
	{
		// se tiver acesso direto a pagina, o usuário é redirecionado para a view criar.
		if ($this->request->getMethod() !== 'post') {
			return redirect()->to('criar');
		}

		$val = $this->validate([
			'nome' => 'required|min_length[3]|max_length[40]',
			'resumo' => 'required|min_length[3]|max_length[100]'
		], [
			'nome' => [
				'required' => 'Você deve informar o nome da disciplina.',
				'min_length' => 'O tamanho mínimo para o nome da disciplina é de 3 caracteres.',
				'max_length' => 'O tamanho máximo para o nome da disciplina é de 40 caracteres.'
			],

			'resumo' => [
				'required' => 'Você deve informar o resumo da disciplina.',
				'min_length' => 'O tamanho mínimo para o resumo é de 3 caracteres.',
				'max_length' => 'O tamanho máximo para o resumo é de 100 caracteres.'
			]
		]);


		if (!$val) {
			return redirect()->back()->withInput()->with('erro', $this->validator);
		} else {
			//Model materia
			$model = new MateriaModel();
			$nome = $this->request->getPost('nome');
			$resumo = $this->request->getPost('resumo');
			$id_professor = $this->request->getPost('id_professor');
			$id = $this->request->getPost('id');
			$data = [
				'id_disciplina' => $id,
				'id_professor' => $id_professor,  
				'nome_disciplina'  => $nome,
				'resumo_disciplina' =>  $resumo
			];
			// metodo para salvar no banco
			// caso $data passe o id ele da update 
			$model->save($data);
			return redirect()->to('index');

		}
	}

	public function editar($id = null)
	{
		$model =  new MateriaModel();
		$data['id_disciplina'] = $model->listar($id);

		if (empty($data['id_disciplina'])) {
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Matéria não encontrada.');
		}

		$data = [
			'nome' => $data['id_disciplina']['nome_disciplina'],
			'resumo' => $data['id_disciplina']['resumo_disciplina'],
			'id_professor' => $data['id_disciplina']['id_professor'],
			'id' => $data['id_disciplina']['nome_disciplina']
		];

		//$model->save($data);
		// $model->update($id, $data);

		echo view('commom/header');
		echo view('materia/editar', $data);
		echo view('commom/footer');
	}

	 public function update(){
        $model = new MateriaModel();
        $id = $this->request->getVar('id_disciplina');
        $data = [
            'nome' => $this->request->getVar('nome_disciplina'),
			'resumo'  => $this->request->getVar('resumo_disciplina')
		];
		$this->load(model('MateriaModel'));
		$this->MateriaModel->updateMateria($id , $data);
        //$model->update($id, $data);
		//return $this->response->redirect(site_url('/Professor'));
		
		echo view('commom/header');
	 	echo view('materia/editar', $data);
	 	echo view('commom/footer');
	}
	
	public function deletar($id = null)
	{

		$model = new MateriaModel();
		$model->delete($id);

		echo view('commom/header');
	 	echo view('materia/deletado_sucesso');
	 	echo view('commom/footer');

		// return redirect()->to('index');
		// return header("location: materia");
        // exit();
	}


	
}


