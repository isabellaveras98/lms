<?php

namespace App\Controllers;

use App\Models\LoginModel;

class Login extends BaseController
{

	public function index()
	{

		$data = [];
		helper(['form']);

		echo view('login/header', $data);
		echo view('login/footer');
	}

	public function login()
	{

		helper(['form']);

		if ($this->request->getMethod() == 'post') {

			$model = new LoginModel();
			$userForm = [
				'email' => $this->request->getVar('email'),
				'password' => $this->request->getVar('password'),
			];

			$user = $model->ValidaLogin($userForm);

			$this->setUserSession($user);

			if ($model->verificaTipo($user)) {
				return redirect()->to('http://localhost/lms/public/Aluno');
			} else {
				return redirect()->to('http://localhost/lms/public/Professor');
			}
		}
	}

	private function setUserSession($user)
	{

		$model = new LoginModel();

		if ($model->verificaTipo($user)) {
			$data = [
				'id' => $user[0]['id_usuario'],
				'email' => $user[0]['login_usuario'],
				'senha' => $user[0]['senha_usuario'],
				'id_aluno' => $user[0]['id_aluno'],
				'isLoggedIn' => true,
			];
		}
		else{
			$data = [
				'id' => $user[0]['id_usuario'],
				'email' => $user[0]['login_usuario'],
				'senha' => $user[0]['senha_usuario'],
				'id_professor' => $user[0]['id_professor'],
				'isLoggedIn' => true,
			];
		}

		session()->set($data);
		return true;
	}

	/* public function profile()
	{

		$data = [];
		helper(['form']);
		$model = new UserModel();

		if ($this->request->getMethod() == 'post') {
			//let's do the validation here
			$rules = [
				'firstname' => 'required|min_length[3]|max_length[20]',
				'lastname' => 'required|min_length[3]|max_length[20]',
			];

			if ($this->request->getPost('password') != '') {
				$rules['password'] = 'required|min_length[8]|max_length[255]';
				$rules['password_confirm'] = 'matches[password]';
			}


			if (!$this->validate($rules)) {
				$data['validation'] = $this->validator;
			} else {

				$newData = [
					'id' => session()->get('id'),
					'firstname' => $this->request->getPost('firstname'),
					'lastname' => $this->request->getPost('lastname'),
				];
				if ($this->request->getPost('password') != '') {
					$newData['password'] = $this->request->getPost('password');
				}
				$model->save($newData);

				session()->setFlashdata('success', 'Successfuly Updated');
				return redirect()->to('/profile');
			}
		}

		$data['user'] = $model->where('id', session()->get('id'))->first();
		echo view('templates/header', $data);
		echo view('profile');
		echo view('templates/footer');
	} */

	public function logout()
	{
		session()->destroy();
		return redirect()->to('http://localhost/lms/public/Login');
	}

	//--------------------------------------------------------------------

}
