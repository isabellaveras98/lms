<?php

namespace App\Controllers;

use App\Models\{ProvaModel, ProvaAlunoModel, QuestaoModel, DisciplinaModel};


class ProvaAluno extends BaseController
{

	public function criar()
	{
		echo view('commom/header');
		echo view('prova/criar');
		echo view('commom/footer');
	}

	public function index()
	{
		echo view('commom/header');
		echo view('aluno/prova');
		echo view('commom/footer');
	}
}
