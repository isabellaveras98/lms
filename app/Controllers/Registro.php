<?php

namespace App\Controllers;

use App\Models\LoginModel;

class Registro extends BaseController
{

    public function index()
    {
        $data = [];
        helper(['form']);

        echo view('registro/header', $data);
        echo view('registro/footer');
    }

    public function register()
    {

        helper(['form']);

        if ($this->request->getMethod() == 'post') {

            $model = new loginModel();

            $newData = [
                'nome' => $this->request->getVar('nome'),
                'lastname' => $this->request->getVar('lastname'),
                'email' => $this->request->getVar('email'),
                'radio' => $this->request->getVar('radio'),
                'password' => $this->request->getVar('password'),
                'password_confirm' => $this->request->getVar('password_confirm'),
            ];

            if (!empty($newData['email'])) {
                if (!empty($newData['nome'])) {
                    if ($newData['password'] == $newData['password_confirm']) {
                        $user = $model->Registra($newData);
                        if ($user != '') {
                            return redirect()->to('http://localhost/lms/public/Login');
                        } else {
                            echo 'Erro, tente novamente';
                        }
                    } else {
                        echo 'Senha incorreta';
                        echo '</br><a href="http://localhost/lms/public/Registro">Voltar</a>';
                    }
                }
                else{
                    echo 'Dados em branco, favor preencher';
                    echo '</br><a href="http://localhost/lms/public/Registro">Voltar</a>';
                }
            } else {
                echo 'Dados em branco, favor preencher';
                echo '</br><a href="http://localhost/lms/public/Registro">Voltar</a>';
            }
        }
    }
}
