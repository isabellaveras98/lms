<?php

namespace App\Controllers;

use App\Models\MatriculadosModel;


class Disciplina extends BaseController
{
	public function index()
	{
	
		$model =  new MatriculadosModel();
		$model->select("id_matriculados, id_aluno, id_disciplina");
		$matriculas = $model->listar();

		foreach ($matriculas as $matricula) {
			$result[] = [
				$matricula['id_matriculados'],
				$matricula['id_aluno'],
				$matricula['id_disciplina']
			];
		}

		$data  =  [
			'data' => $matriculas
		];

		echo view('commom/header');
		echo view('disciplina/index', $data);
		echo view('commom/footer');

    }

    public function criar()
	{
		$data = [];
		if (session()->has('erro')) {
			$data['erro'] = session('erro');
		}
		echo view('commom/header');
		echo view('disciplina/criar', $data);
		echo view('commom/footer');
	}

    public function Salvar($id = null)
	{
		// se tiver acesso direto a pagina, o usuário é redirecionado para a view criar.
		if ($this->request->getMethod() !== 'post') {
			return redirect()->to('criar');
		}

		$val = $this->validate([
			'nome' => 'required|min_length[3]|max_length[40]',
			'aluno' => 'required|min_length[1]|max_length[3]'
		], [
			'nome' => [
				'required' => 'Você deve informar o nome da disciplina.',
				'min_length' => 'O tamanho mínimo para o nome da disciplina é de 3 caracteres.',
				'max_length' => 'O tamanho máximo para o nome da disciplina é de 40 caracteres.'
			],

			'resumo' => [
				'required' => 'Você deve informar id do aluno.',
				'min_length' => 'O tamanho mínimo para o resumo é de 3 caracteres.',
				'max_length' => 'O tamanho máximo para o resumo é de 100 caracteres.'
			]
		]);


		if (!$val) {
			return redirect()->back()->withInput()->with('erro', $this->validator);
		} else {
			//Model materia
			$model = new MatriculadosModel();
			$nome = $this->request->getPost('nome');
			$aluno = $this->request->getPost('aluno');
			$data = [
				'id_disciplina' => $nome,  
				'id_aluno'  => $aluno,
			];
			// metodo para salvar no banco
			// caso $data passe o id ele da update 
			$model->save($data);
			return redirect()->to('index');

		}
	}

	public function editar($id = null)
	{
		$model =  new MatriculadosModel();
		$data['id_matriculados'] = $model->listar($id);

		if (empty($data['id_matriculados'])) {
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Matricula não encontrada.');
		}

		$data = [
			'id' => $data['id_discipid_matriculadoslina'],
			'nome' => $data['id_disciplina'],
			'aluno' => $data['id_aluno']
		];

		//$model->save($data);
		// $model->update($id, $data);

		echo view('commom/header');
		echo view('disciplina/editar', $data);
		echo view('commom/footer');
	}

/* 	 public function update(){
        $model = new DisciplinaModel();
        $id = $this->request->getVar('id_matriculados');
        $data = [
            'nome' => $this->request->getVar('id_disciplina'),
			'aluno'  => $this->request->getVar('id_aluno')
		];
		$this->load(model('DisciplinaModel'));
		$this->MateriaModel->updateMateria($id , $data);
        //$model->update($id, $data);
		//return $this->response->redirect(site_url('/Professor'));
		
		echo view('commom/header');
	 	echo view('disciplina/editar', $data);
	 	echo view('commom/footer');
	} */
	
	public function deletar($id = null)
	{

		$model = new MatriculadosModel();
		$model->delete($id);

		echo view('commom/header');
	 	echo view('disciplina/deletado_sucesso');
	 	echo view('commom/footer');

		// return redirect()->to('index');
		// return header("location: materia");
        // exit();
	}


	
}


