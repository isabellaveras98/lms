<?php namespace App\Controllers;

use App\Models\AlunoModel;

class Aluno extends BaseController {

    public function index(){
		  $model = new AlunoModel();
      $dados['tbconteudo'] = $model->materias();
      $dados['tbatividadealuno'] = $model->atividades();
      // $dados['tbprovaaluno'] = $model->provas();

      //usort($dados['tbprovaaluno'], array($this, 'date_compare')); 
      
      echo view('aluno/header');
      echo view('aluno/sidebar');
		  echo view('aluno/page', $dados);
		  echo view('aluno/footer');
    }

    /* private function date_compare($element1, $element2) { 
      $datetime1 = strtotime($element1['data_provaluno']); 
      $datetime2 = strtotime($element2['data_provaluno']); 
      return $datetime1 - $datetime2; 
    } */  
}