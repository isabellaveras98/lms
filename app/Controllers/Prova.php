<?php

namespace App\Controllers;

use App\Models\{ProvaModel, ProvaAlunoModel, QuestaoModel, DisciplinaModel};


class Prova extends BaseController
{

	public function criar()
	{
		$otModel = new DisciplinaModel();
		$model =  new QuestaoModel();

		$otModel->select("id_disciplina, nome_disciplina");
		$disciplinas = $otModel->listar();

		foreach ($disciplinas as $disciplina) {
			$result[] = [
				$disciplina['id_disciplina'],
				$disciplina['nome_disciplina']
			];
		}

		$model->select("id_questao, enunciado");
		$questoes = $model->listar();

		foreach ($questoes as $questao) {
			$result[] = [
				$questao['id_questao'],
				$questao['enunciado']
			];
		}

		$data  =  [
			'data' => $questoes,
			'disciplina' => $disciplinas
		];

		echo view('commom/header');
		echo view('prova/criar', $data);
		echo view('commom/footer');
	}

	public function index()
	{

		echo view('commom/header');
		echo view('prova/index');
		echo view('commom/footer');
	}
	public function aluno()
	{

		echo view('commom/header');
		echo view('prova/aluno');
		echo view('commom/footer');
	}

	public function salvar($id = null)
	{
		$modelProva = new ProvaModel();

		$questao1 = $this->request->getPost('questao1');
		$questao2 = $this->request->getPost('questao2');
		$questao3 = $this->request->getPost('questao3');
		$questao4 = $this->request->getPost('questao4');
		$questao5 = $this->request->getPost('questao5');
		$id_disciplina = $this->request->getPost('id_disciplina');

		$dataProva = [
			'questao1'  => $questao1,
			'questao2' =>  $questao2,
			'questao3' =>  $questao3,
			'questao4' =>  $questao4,
			'questao5' =>  $questao5,
			'id_disciplina' =>  $id_disciplina

		];
		// metodo para salvar no banco
		// caso $data passe o id ele da update 


		$modelProva->save($dataProva);

		return redirect()->to('http://localhost/lms/public/Prova');
	}

	public function atribuir($id = null)
	{
		$modelProva = new ProvaAlunoModel();

		$aluno = $this->request->getPost('aluno');
		$prova = $this->request->getPost('prova');
		$realizada = 'N';
		$respostas = null;
		$nota = 0;

		$dataProva = [
			'id_prova'  => $prova,
			'id_aluno' =>  $aluno,
			'realizada' => $realizada,
			'respostas' => $respostas,
			'nota' => $nota

		];
		// metodo para salvar no banco
		// caso $data passe o id ele da update 


		$modelProva->save($dataProva);
		return redirect()->to('/lms/public/prova');
	}

	public function todas()
	{
		$model =  new QuestaoModel();
		$model->select("id_questao, enunciado, tag");
		$questoes = $model->listar();

		foreach ($questoes as $questao) {
			$result[] = [
				$questao['id_questao'],
				$questao['enunciado'],
				$questao['tag']
			];
		}

		$data  =  [
			'data' => $questoes
		];


		echo view('commom/header');
		echo view('prova/todas', $data);
		echo view('commom/footer');
	}
}
