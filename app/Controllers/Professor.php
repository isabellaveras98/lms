<?php

namespace App\Controllers;

use App\Models\ProfessorModel;
use App\Models\UsuarioModel;


class Professor extends BaseController
{
	public function index()
	{
		// model porfessor
		$model =  new ProfessorModel();
		$model->select("nome_professor, id_professor");
		$usuarios = $model->listar();

		foreach ($usuarios as $usuario) {
			$result[] = [
				$usuario['nome_professor'],
				$usuario['id_professor']
			];
		}

		$data  =  [
			'data' => $usuarios
		];

		echo view('commom/header');
		echo view('professor/index', $data);
		echo view('commom/footer');

		// model usuario
		// $model = new UsuarioModel();
		// $usuarios = $model->ListarPorTipo(1);

		// $model->select("id_usuario,login_usuario, tipo");
		// $usuarios = $model->listar();

		// foreach ($usuarios as $usuario) {
		// 	$result[] = [
		// 		$usuario['login_usuario'],
		// 		$usuario['id_usuario'],
		// 		$usuario['tipo']
		// 	];
		// }

		// $data  =  [
		// 	'data' => $usuarios
		// ];

		// echo view('commom/header');
		// echo view('professor/index', $data);
		// echo view('commom/footer');


	}

	public function visualizar($id = null)
	{
		// Model professor
		$model =  new ProfessorModel();
		$data['id_professor'] = $model->listar($id);

		//  Logica para saber se a pagina vai ser encontrada se nao ele coloca uma mendagem padrao
		if (empty($data['id_professor'])) {
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Professor Não Encontrado');
		}
		// array com os dados do banco para a view
		$data = [
			'nome' => $data['id_professor']['nome_professor'],
			'login' => $data['id_professor']['login_professor'],
		];

		echo view('commom/header');
		echo view('professor/Visualizar', $data);
		echo view('commom/footer');

		// Model Usuario
		// $model =  new UsuarioModel();
		// $data['id_usuario'] = $model->listar($id);

		// //  Logica para saber se a pagina vai ser encontrada se nao ele coloca uma mendagem padrao
		// if (empty($data['id_usuario'])) {
		// 	throw new \CodeIgniter\Exceptions\PageNotFoundException('Professor Não Encontrado');
		// }
		// // array com os dados do banco para a view
		// $data = [
		// 	'nome' => $data['id_usuario']['login_usuario'],
		// 	'tipo' => $data['id_usuario']['tipo'],
		// ];

		// echo view('commom/header');
		// echo view('professor/Visualizar', $data);
		// echo view('commom/footer');
	}

	public function editar($id = null)
	{
		$model =  new ProfessorModel();
		$data['id_professor'] = $model->listar($id);

		//  Logica para saber se a pagina vai ser encontrada se nao ele coloca uma mendagem padrao
		if (empty($data['id_professor'])) {
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Professor Não Encontrado');
		}

		$data = [
			'nome' => $data['id_professor']['nome_professor'],
			'login' => $data['id_professor']['login_professor'],
			'id' => $data['id_professor']['login_professor']
		];

		echo view('commom/header');
		echo view('professor/Editar', $data);
		echo view('commom/footer');

		// Model Usuario
		// $model =  new UsuarioModel();
		// $data['id_usuario'] = $model->listar($id);

		// //  Logica para saber se a pagina vai ser encontrada se nao ele coloca uma mendagem padrao
		// if (empty($data['id_usuario'])) {
		// 	throw new \CodeIgniter\Exceptions\PageNotFoundException('Professor Não Encontrado');
		// }

		// $data = [
		// 	'login' => $data['id_usuario']['login_usuario'],
		// 	'tipo' => $data['id_usuario']['tipo'],
		// 	'id' => $data['id_usuario']['id_usuario']
		// ];

		// echo view('commom/header');
		// echo view('professor/Editar', $data);
		// echo view('commom/footer');

	
	}

	public function criar()
	{
		$data = [];
		if (session()->has('erro')) {
			$data['erro'] = session('erro');
		}
		echo view('commom/header');
		echo view('professor/Criar', $data);
		echo view('commom/footer');
	}

	public function deletar($id = null)
	{
		$model = new ProfessorModel();
		$model->delete($id, 'false');

		return redirect()->to('index');
	}

	public function Salvar($id = null)
	{
		// se tiver acesso direto a pagina ele redireciona para a criar
		if ($this->request->getMethod() !== 'post') {
			return redirect()->to('criar');
		}

		// validacao do formulario Model professor
		$val = $this->validate([
			'nome' => 'required|min_length[3]|max_length[25]',
			'login' => 'required|min_length[3]|max_length[25]',
			'senha' => 'required|min_length[5]|max_length[25]'
		], [
			'nome' => [
				'required' => 'O campo Nome é obrigatório',
				'min_length' => 'O minimo de caracteres é 3',
				'max_length' => 'O maximo de caracteres é 25'
			],

			'login' => [
				'required' => 'O campo Login é obrigatório',
				'min_length' => 'O minimo de caracteres para o login é 3',
				'max_length' => 'O maximo de caracteres par o login é 25'
			],

			'senha' => [
				'required' => 'O campo Senha é obrigatório',
				'min_length' => 'O minimo de caracteres é 3',
				'max_length' => 'O maximo de caracteres é 25'
			]
		]);


		// $val = $this->validate([
		// 	'nome' => 'required|min_length[3]|max_length[25]',
		// 	'login' => 'required|min_length[1]|max_length[1]',
		// ], [
		// 	'nome' => [
		// 		'required' => 'O campo Nome é obrigatório',
		// 		'min_length' => 'O minimo de caracteres é 3',
		// 		'max_length' => 'O maximo de caracteres é 25'
		// 	],

		// 	'login' => [
		// 		'required' => 'O campo Login é obrigatório',
		// 		'min_length' => 'O minimo de caracteres para o login é 3',
		// 		'max_length' => 'O maximo de caracteres par o login é 25'
		// 	],

		
		// ]);

		if (!$val) {
			return redirect()->back()->withInput()->with('erro', $this->validator);
		} else {
			//Model professor
			$model = new ProfessorModel();
			$nome = $this->request->getPost('nome');
			$login = $this->request->getPost('login');
			$senha = $this->request->getPost('senha');
			$id = $this->request->getPost('id');
			$data = [
				'id_professor' => $id,
				'nome_professor'  => $nome,
				'login_professor' =>  $login,
				'senha_professor' => password_hash($senha, PASSWORD_DEFAULT)
			];
			// metodo para salvar no banco
			// caso $data passe o id ele da update 
			$model->save($data);
			return redirect()->to('index');

			//Model Usuario
			// $model = new UsuarioModel();
			// $nome = $this->request->getPost('nome');
			// $tipo = $this->request->getPost('login');
			// $id = $this->request->getPost('id');
			// $data = [
			// 	'id_usuario' => $id,
			// 	'login_usuario'  => $nome,
			// 	'tipo' =>  $tipo
			// ];
			// // metodo para salvar no banco
			// // caso $data passe o id ele da update 
			// $model->save($data);
			// return redirect()->to('index');
		}
	}
}
