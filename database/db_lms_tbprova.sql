CREATE DATABASE  IF NOT EXISTS `db_lms` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `db_lms`;
-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: db_lms
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbprova`
--

DROP TABLE IF EXISTS `tbprova`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbprova` (
  `id_prova` int NOT NULL AUTO_INCREMENT,
  `questao1` int DEFAULT NULL,
  `questao2` int DEFAULT NULL,
  `questao3` int DEFAULT NULL,
  `questao4` int DEFAULT NULL,
  `questao5` int DEFAULT NULL,
  `id_disciplina` int NOT NULL,
  PRIMARY KEY (`id_prova`),
  KEY `questao1` (`questao1`),
  KEY `questao2` (`questao2`),
  KEY `questao3` (`questao3`),
  KEY `questao4` (`questao4`),
  KEY `questao5` (`questao5`),
  CONSTRAINT `tbprova_ibfk_4` FOREIGN KEY (`questao1`) REFERENCES `tbquestao` (`id_questao`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tbprova_ibfk_5` FOREIGN KEY (`questao2`) REFERENCES `tbquestao` (`id_questao`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tbprova_ibfk_6` FOREIGN KEY (`questao3`) REFERENCES `tbquestao` (`id_questao`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tbprova_ibfk_7` FOREIGN KEY (`questao4`) REFERENCES `tbquestao` (`id_questao`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tbprova_ibfk_8` FOREIGN KEY (`questao5`) REFERENCES `tbquestao` (`id_questao`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbprova`
--

LOCK TABLES `tbprova` WRITE;
/*!40000 ALTER TABLE `tbprova` DISABLE KEYS */;
INSERT INTO `tbprova` VALUES (10,18,19,20,21,22,3);
/*!40000 ALTER TABLE `tbprova` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-05 23:50:38
