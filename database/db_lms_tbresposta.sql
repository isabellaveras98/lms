CREATE DATABASE  IF NOT EXISTS `db_lms` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `db_lms`;
-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: db_lms
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbresposta`
--

DROP TABLE IF EXISTS `tbresposta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbresposta` (
  `id_resposta` int NOT NULL AUTO_INCREMENT,
  `id_questao` int NOT NULL,
  `certa` varchar(1) NOT NULL,
  `resposta` varchar(4025) NOT NULL,
  PRIMARY KEY (`id_resposta`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbresposta`
--

LOCK TABLES `tbresposta` WRITE;
/*!40000 ALTER TABLE `tbresposta` DISABLE KEYS */;
INSERT INTO `tbresposta` VALUES (11,18,'N','1'),(12,18,'N','3'),(13,18,'N','5'),(14,18,'S','4'),(15,19,'N','Argentina'),(16,19,'S','Brasil'),(17,19,'N','Colombia'),(18,19,'N','Chile'),(19,20,'S','EUA'),(20,20,'S','Canada'),(21,20,'N','Alaska'),(22,20,'N','Mexico'),(23,21,'S','Sim'),(24,21,'N','Não'),(25,21,'N','Talvez'),(26,21,'N','Slá'),(27,22,'N','Dere'),(28,22,'S','Ha'),(29,22,'N','Derebetubere'),(30,22,'N','jfhkawyfgebwky');
/*!40000 ALTER TABLE `tbresposta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-05 23:50:38
