--delecao de dados, caso tenha algum dados

DELETE FROM `tbprovaaluno` ;
DELETE FROM `tbprovaprof` ;

--ALTERA��ES NO BANCO

ALTER TABLE db_lms.tbprovaprof DROP FOREIGN KEY `tbprovaprof_ibfk_2`;
ALTER TABLE `tbprovaprof` DROP `id_disciplina`;
ALTER TABLE db_lms.tbprovaprof DROP FOREIGN KEY `tbprovaprof_ibfk_1`;
ALTER TABLE `tbprovaprof` DROP `id_conteudo`;
DELETE FROM `tbprovaprof` ;
ALTER TABLE `tbprovaprof` CHANGE `questoes` `questao1` INT(10) NULL;
ALTER TABLE `tbprovaprof` ADD `questao2` INT(3) NULL AFTER `questao1`, ADD `questao3` INT(3) NULL AFTER `questao2`, ADD `questao4` INT(3) NULL AFTER `questao3`, ADD `questao5` INT(3) NULL AFTER `questao4`;
CREATE TABLE `db_lms`.`tbquestao` ( `id_questao` INT(3) NULL AUTO_INCREMENT , `tag` VARCHAR(255) NULL , `enunciado` VARCHAR(4025) NOT NULL , PRIMARY KEY (`id_questao`)) ENGINE = InnoDB;
ALTER TABLE `tbquestao` CHANGE `tag` `tag` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL;
CREATE TABLE `db_lms`.`tbresposta` ( `id_resposta` INT(3) NOT NULL AUTO_INCREMENT , `id_questao` INT(3) NOT NULL , `certa` VARCHAR(1) NOT NULL , PRIMARY KEY (`id_resposta`)) ENGINE = InnoDB;
ALTER TABLE `tbprovaaluno` DROP `data_provaluno`;

ALTER TABLE `tbprovaprof` ADD FOREIGN KEY (`questao1`) REFERENCES `tbquestao`(`id_questao`) 
ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `tbprovaprof` ADD FOREIGN KEY (`questao2`) REFERENCES `tbquestao`(`id_questao`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE `tbprovaprof` ADD FOREIGN KEY (`questao3`) REFERENCES `tbquestao`(`id_questao`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE `tbprovaprof` ADD FOREIGN KEY (`questao4`) REFERENCES `tbquestao`(`id_questao`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE `tbprovaprof` ADD FOREIGN KEY (`questao5`) REFERENCES `tbquestao`(`id_questao`) ON DELETE RESTRICT ON UPDATE RESTRICT;


ALTER TABLE `tbresposta` ADD FOREIGN KEY (`id_questao`) REFERENCES `tbquestao`(`id_questao`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `tbresposta` ADD `resposta` VARCHAR(4025) NOT NULL AFTER `certa`;

ALTER TABLE db_lms.tbprovaprof DROP FOREIGN KEY `tbprovaprof_ibfk_3`;
ALTER TABLE `tbprovaprof` DROP `id_professor`;
ALTER TABLE db_lms.tbprovaaluno DROP FOREIGN KEY `tbprovaaluno_ibfk_2`;
ALTER TABLE `tbprovaaluno` DROP `id_provaprof`;
ALTER TABLE `tbprovaaluno` ADD `id_prova` INT(3) NOT NULL AFTER `nota`;
ALTER TABLE `tbprovaaluno` ADD FOREIGN KEY (`id_prova`) REFERENCES `tbprovaprof`(`id_provaprof`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `tbprovaaluno` CHANGE `realizada` `realizada` VARCHAR(1) NOT NULL;
ALTER TABLE `tbprovaaluno` CHANGE `respostas` `respostas` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL;
ALTER TABLE `tbprovaaluno` CHANGE `nota` `nota` INT(255) NULL;




--inser��o de dados, caso queira
INSERT INTO `tbquestao` (`id_questao`, `tag`, `enunciado`) VALUES (NULL, 'Fun��o', 'What is Lorem Ipsum?'), 
(NULL, '�lgebra', 'Why do we use it?'), 
(NULL, 'Probabilidade', 'Why do we use it?'), 
(NULL, 'Aritm�tica ', 'Where can I get some?'), 
(NULL, 'Geometria', 'Integer pellentesque aliquet urna, vitae cursus lectus lacinia eu?');

